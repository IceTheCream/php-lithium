<?php

//require("system.php");

class CEasyConfig {
 
    private $sconfigname="";    
    private $xmldoc;
    private $xpath;
    
    private $sserver="";
    private $sdatabase="";
    private $suser="";
    private $spassword="";
    private $slogname="";
    private $scodepage="";
        
    public function __construct( $psconfigname) {
        
        global $osyslog;
        
        $this->sconfigname=$psconfigname;
        if (file_exists($this->sconfigname)) {

            $this->xmldoc=simplexml_load_file($this->sconfigname);

            $this->sserver=trim($this->xmldoc->mysql->server);
            $this->sdatabase=trim($this->xmldoc->mysql->database);
            $this->suser=trim($this->xmldoc->mysql->user);
            $this->spassword=trim($this->xmldoc->mysql->password);
            $this->scodepage=trim($this->xmldoc->mysql->codepage);
            $this->slogname=trim($this->xmldoc->system->logname);
        }   else {
            global $osyslog;
            $SystemLog->writeln('Given XML is not found!');
        }
    }
    
        //echo '<pre>';
        //var_dump($this->xmldoc->mysql);
        //echo '</pre>';

    
    public function __destruct() {
        
        //unset($this->xpath);
        unset($this->xmldoc);
    }


    public function getServer() {
        
        return $this->sserver;
    }

    
    public function getDatabase() {
        
        return $this->sdatabase;
    }


    public function getUser() {
        
        return $this->suser;
    }


    public function getPassword() {
        
        return $this->spassword;
    }


    public function getLogName() {
        
        return $this->slogname;
    }
    

    public function getCodePage() {
        
        return $this->scodepage;
    }
    

}

?>
