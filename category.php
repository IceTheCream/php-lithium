<?php

class CCategoryBuilder extends CDocument {
 
    private $mxmlname="";
    private $mconnect=null;
    private $msectionsinrow=0;
    private $mclassname="";
    //private $msqlselect="";
    private $msqlcount="";
    private $Query=null;
    
    public function __construct($pconnect,$pxmlname) {
     
        parent::__construct();   
       $this->mxmlname=$pxmlname;
       $this->mconnect=$pconnect;
         
    }
    
    
    private function readConfig() {
        
        $this->msectionsinrow=$xml->category->sectionsinrow;
        $this->mclassname=$xml->category->classname;
        //$this->msectionsql=$xml->section->sql; //???
        $this->msqlselect=$xml->category->sqlselect;
        $this->msqlcount=$xml->category->sqlcount;
        $this->mcodepage=$xml->codepage;
        parent::setCondensed($xml->condensed);    
    }
    
    
    public function build($pcategoryid) {
        
        parent::clean();
        $this->mcategoryid=$pcategoryid;
        
        //*** 1. Определиться, сколько у нас секций в этой категории
        $this->Query=new CDBQuery($this->mconnect,$this->mcodepage); 
        if($this->Query->open($this->msqlselect.$this->mcategoryid)) {
       
            $reccount=$this->Query->recordCount();

            //*** Получаем массив айдишников.
            $SectionIDArr=$this->Query->getDBSlice('id');
            $currentSection=0;
            //*** 2. Подсчитать кол-во строк
            $rows=ceil($reccount/$this->$msectionsinrow);

            //*** 3. Создать таблицу
            buildTableHeader();

            //*** 4. Открыть цикл по к-ву строк секций
            for($idx=0;$idx<$rows;$idx++) {

                //*** 5. Вызываем процедуру buildRow.
                $currentSection=buildRow($SectionIDArr, $currentSection);
        
            //*** 7. Закроем цикл. Готово.
            }
            
            
            
            
                // тут определиться с количеством строк в секциях для задания высоты.
                // А вот тут из этого массива билдеров считать массив количества записей в каждой секции 25.02.2014
                //$sectionrows=5
                // А вот тут билдить их по очереди  25.02.2014
                //$SectionBuilder->build(1,5);
                //$SectionBuilder->display();
                //$SectionBuilder->build(2,5);
                //$SectionBuilder->display();
                
            //}
        }
    }
    
    public function buildRow($pSectionIDArr,$pcurrentSection) {
        
        //*** 1. Завести массив билдеров секций размером $this->$msectionsinrow и такой же массив размеров секций
        $SectionBuilderArr = array();
        $SectionSizeArr = array();
        
        //*** 3. Запросить к-во строк для всех секций в строке таблицы в массивы
        for($Idx=0;$Idx<$this->$msectionsinrow;$Idx++) {
            
            $SectionBuilderArr[$Idx]=new CSoftwareSectionBuilder($this->mconnect,$this->mxmlname);
            $SectionSizeArr[$Idx]=$SectionBuilderArr[$Idx].askRows($pSectionIDArr[$pcurrentSection]);
            $pcurrentSection++;
        }
        
        
        
        
        //*** 5. Определить самую длинную секцию для текущей строки
        //*** 6. Вывести строку секций, передавая им макс. высоту для этой строки
            
            //Тут бы  массив билдеров завести 25.02.2014 
            // а сколько ж у нас секций?
            //! $SectionBuilder=
        //Unset
        return $pcurrentSection;
    }
        
    private function buildTableHeader() {

            //***** Заголовок таблицы.
            $this->addln("<table class=\"{$this->mclassname}\">");    
            $this->addln("<colgroup>");
            
            //***** Рассчитаем ширины столбцов
            $colwidth=floor(100/$this->msectionsinrow);
            for($idx=0;$idx<$this->msectionsinrow;$idx++) {

                $this->addln("<col width=\"{$colwidth}\">");
            }        
            $this->addln("</colgroup>");
            $this->addln("<tbody>");
            $this->addln("");
    }

    private function buildTableFooter() {
        
            $this->addln("</tbody>");
            $this->addln("</table>");    
    }
}

?>
