<?php

class CDBQuery  {

    private $Connection=null;
    private $mqueryid=null;
    private $msqltext="";
    
    public function __construct($pdbconnect, $pcodepage="utf8") {
        
        $this->Connection=$pdbconnect;
        $this->Connection->query("set names ".$pcodepage);
    }
    
    
    public function initialize($pSQL) {
        
        $this->msqltext=$pSQL;
    }
    
    public function open($pSQL="") {
    
        if(!isEmpty($pSQL)) {
            
            $this->msqltext=$pSQL;
        } 
        
        $this->mqueryid=$this->Connection->query($this->msqltext);
        return $this->mqueryid;
    }

    
    public function close() {
    
        mysql_free_result($this->mqueryid);
    }
    
    
    public function recordCount() {
    
        return mysql_num_rows($this->mqueryid);
    }
  
    
    public function getRecord() {
    
        return mysql_fetch_array($this->mqueryid);
    }
    
    
    public function getDBSlice($pFieldName) {

        $reccount=recordCount();
        $recArr=Array();
        $rec=Array();
        
        for($idx=0;$idx<$reccount;$idx++) {

            $rec=$this->Query->getRecord($idx);
            $recArr[$idx]=$rec[$pFieldName];
        }
        return $recArr;
    }
    
    public function addParameter($pName, $pValue) {
     
        $sql=findAndReplace($this->msqltext,$pName,$pValue);
        
    }
    
}

?>
