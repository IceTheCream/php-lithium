<?php


function isEmpty($pLine) {
    return strlen(trim($pLine))==0;
}

    
function findAndReplace($pLine,$pSearchStr,$pSubStr) {
    
    $len=strlen($pSearchStr);
    $pos=stripos($pLine,$pSearchStr);
    $sub=substr($pLine,$pos,$len);
    if(strcasecmp($sub,$pSubStr)) {
        
        return substr_replace($pLine,$pSubStr,$pos,$len);    
    } else {
        
        return "";    
    }
}
    
function out($psLine) {

    echo "$psLine";
}


function outln($psLine) {

    out($psLine."\n");
}


function outbr($psLine) {

    out($psLine."<br>");
}


function dout($psLine) {

    global $idebug;
    if($idebug) {
        
        outbr("<div class=\"debug\">[".$psLine."]</div>");
    }
}
  
?>
