<?php

class CEasySideMenu extends CDocument {
    
    private $mxmlname="";

    private $mcssfilename="";
    private $mclassname="";
    private $mevenlineid="";
    private $moddlineid="";
    private $mheaderclass="";
    private $mtitle="";
    private $mpadding="";
    private $msql="";
    private $Connect=null;
    private $Query=null;
               
    public function __construct($pxmlname,$pconnect) {
     
        //parent::__construct($picondensed);    
        $this->mxmlname=$pxmlname;
        $this->Connect=$pconnect;
        $this->readConfig();
    }
    
    private function readConfig() {
        
        $xml=simplexml_load_file($this->mxmlname);
        $this->mclassname=$xml->css->classname;
        $this->mevenlineid=$xml->css->evenlineid;
        $this->moddlineid=$xml->css->oddlineid;
        $this->mheaderclass=$xml->css->headerclass;
        $this->mtitle=$xml->title;
        $this->mpadding=$xml->padding;
        $this->msql=$xml->sql;
        parent::setCondensed($xml->condensed);
        unset($xml);    
    }
    
        
    public function build() {
        
        $this->Query=new CDBQuery($this->Connect);
        if($this->Query->open($this->msql)) {
        
            $itemscount=$this->Query->recordcount();
            $this->addln("");
            $this->addln("<table class=\"{$this->mclassname}\">");
            $this->addln("<colgroup>");
            $this->addln("<col width=\"{$this->mpadding}\">");
            $this->addln("<col width=\"*%\">");
            $this->addln("<col width=\"{$this->mpadding}\">");
            $this->addln("</colgroup>");
            $this->addln("<thead id=\"{$this->mheaderclass}\">");
            $this->addln("<tr><th colspan=3>{$this->mtitle}</th></tr>");
            $this->addln("</thead>");
            $this->addln("<tbody>");

            for($idx=0;$idx<$itemscount;$idx++) {
                    
                $record=$this->Query->getRecord($idx);
                if($idx%2>0) { 
 
                    $this->buildItem($record,$this->moddlineid);
                }  else {
 
                    $this->buildItem($record,$this->mevenlineid);
                }
                $this->addln("");
            }   
            $this->add("</tbody>");
            $this->addln("</table>");
            $this->addln("");
            
        }    
    }


    public function buildItem($precord,$plineid) {
        
        $this->addln("<tr id=\"{$plineid}\">");
        $this->add("<td></td><td><a href=\"");
        $this->add($precord["alink"]);
        if(!is_null($precord["acat"])) {
    
            $this->add("?cat=".$precord["acat"]);
        }
        $this->add("\">".$precord["aname"]);
        $this->addln("</a></td><td></td>");
        $this->addln("</tr>");   
    }    
}
    
?>
