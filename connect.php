<?php

class CDBConnect  {

    private $maddress="";
    private $muser="";
    private $mpassword="";
    private $mdatabase="";
    private $mhandle;
    private $merrormsg="";
    
    /*

    private $mysqli=null;
   
    private $codepage="utf8";
     * */
    
    
    public function __construct($pdatabase) {

        $this->mdatabase=$pdatabase;
    }
    
    
    public function setAddress($paddress) {
 
        $this->maddress=$paddress;
    }
    
    
    public function setDatabase($pdatabase) {
 
        $this->mdatabase=$pdatabase;   
    }
    
    
    public function setUser($puser) {
  
        $this->muser=$puser;
    }


    public function setPassword($ppassword) {
 
        $this->mpassword=$ppassword;    
    }
    
    
    public function isParametersCorrect() {
        
        if (!(isEmpty($this->maddress) or 
              isEmpty($this->mdatabase) or 
              isEmpty($this->muser) or 
              isEmpty($this->mpassword)
            )) {
            return 1;    
        } else {
            
            return 0;
        }        
    }
    
    public function open() {
    
        //***** Все ли параметры заданы?
        if(isParametersCorrect()) {   
          $this->mhandle=mysql_connect($this->maddress, $this->muser, $this->mpassword);  
          $this->merrormsg=mysql_error();
        }    
        return $this->mhandle;  
    }


    public function selectdb() {
        
        $result=mysql_select_db($this->mdatabase, $this->mhandle);
        $this->merrormsg=mysql_error();
        return $result;
    }


    public function query($pquery) {

        $result=mysql_query($pquery, $this->mhandle);
        $this->merrormsg=mysql_error();
        return $result;
    }    
    
    
    public function close() {
        
        return mysql_close($this->mhandle);    
    }
    
    
    public function geterror() {
    
        return $this->merrormsg;
    }
    
    
    public function __destruct() {
        
        if ($this->mhandle) {

            mysql_close($this->mhandle);
        }    
    }   
    
}

?>
