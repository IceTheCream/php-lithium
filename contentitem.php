<?php

class CSoftwareItem extends CPageObject {
    
    const FIELD_SECTION="fsection";
    const FIELD_NAME="fname";
    const FIELD_VERSION="fversion";
    const FIELD_LICENSE="flicense";
    const FIELD_URL="furl";
    const FIELD_PORTABLE="fportableurl";
    const FIELD_DESCRIPTION="fdescription";

    private $record;
    private $style="";
    
    public function __construct($porecord,$psstyle,$picondensed=0) {
        
        parent::__construct($picondensed);
        $this->record=$porecord;
        $this->style=$psstyle;
    }
    
    public function build() {
        
        //Все это замечательно, но нужно же и стили применить как-то?
        // А так нормально идет ;)
        $this->add("<tr>");
        $this->add("<td>{$record[FIELD_NAME]}</td>");
        $this->add("<td>{$record[FIELD_VERSION]}</td>");
        $this->add("<td>{$record[FIELD_LICENSE]}</td>");
        $this->add("<td><a href=\"{$record[FIELD_URL]}\">ссылка</a></td>");
        $this->add("<td><a href=\"{$record[FIELD_PORTABLE]}\">портабль</a></td>");
        $this->add("</tr>");
        //$this->add();
        
    }
    
    
}
?>
