<?php

class CEasyLog extends CDocument {

    private $mlogname="";
    private $mloghandle=0;

    public function __construct($plogname) {

        parent::__construct();
        $this->mlogname=$plogname;
        $this->mloghandle=fopen($this->mlogname,FILE_CREATE_MODE);
    }


    public function writeln($pline) {
    
        fwrite($this->mloghandle, $pline."\n");        
    }

    
    public function printlog() {
 
        $fhandle=fopen($this->mlogname,'rt');
        if ($fhandle) {
        
            echo"<div class=\"log\">";
            while(!feof($fhandle)) {
                
                $line=fgets($fhandle,255);
                if(!isEmpty($line)) {

                    echo $line."<br>";
                }
            }
            echo"</div>";
            fclose($fhandle);
        }
    }
 
    
    public function __destruct() {

        fclose($this->mloghandle);
    }


}

?>
