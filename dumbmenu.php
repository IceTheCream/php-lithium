<?php

class CDumbMenu {
    private $aitems;
    private $aurls;
    private $nitemcount;    

    public function __construct($pnitemcount) {

        $this->aitems=array($pnitemcount);
        $this->aurls=array($pnitemcount);
        $this->nitemcount=$pnitemcount;
    }

    public function add($piindex,$psitem,$psurl) {
  
        $this->aitems[$piindex]=$psitem;
        $this->aurls[$piindex]=$psurl;    
    }

    public function display() {

        echo "<div align=center>";
        for ($idx=0;$idx<$this->nitemcount;$idx++) {
            //echo ":::".$idx;        
            echo "<a href=\"".$this->aurls[$idx]."\">".$this->aitems[$idx]."</a> ";
        }
        echo "</div>";
    }
}
?>
