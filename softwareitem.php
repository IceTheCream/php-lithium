<?php




class CSoftwareItemBuilder extends CDocument {
    
    const FIELD_SECTION="fsection";
    const FIELD_NAME="fname";
    const FIELD_VERSION="fversion";
    const FIELD_LICENSE="flicense";
    const FIELD_URL="furl";
    const FIELD_PORTABLE="fportableurl";
    const FIELD_DESCRIPTION="fdescription";

    //private $mrecord=Array();
    private $mstyle="";
    private $moddlineid="";
    private $mevenlineid="";
    private $mxmlname="";
    private $minstallericon="";
    private $mportableicon="";
    private $miconsdim=16;
    
    public function __construct($pxmlname) {
        
        parent::__construct();
        $this->mxmlname=$pxmlname;
        $this->readConfig();
    }
    
    
      private function readConfig() {
        
        $xml=simplexml_load_file($this->mxmlname);
        $this->mevenlineid=$xml->item->evenlineid;
        $this->moddlineid=$xml->item->oddlineid;
        $this->minstallericon=$xml->item->installericon;
        $this->mportableicon=$xml->item->portableicon;
        $this->miconsdim=$xml->item->icondimension;
       
        parent::setCondensed($xml->condensed);
        unset($xml);    
    }
 
    
    public function build($precord,$prow) {
        
        parent::clean();
        if($prow%2>0) {
            
            $this->addln("<tr id=\"{$this->moddlineid}\">");
        } else {

            $this->addln("<tr id=\"{$this->mevenlineid}\">");
        }
        $this->addln("<td>{$precord[self::FIELD_NAME]}</td>");
        $this->addln("<td>{$precord[self::FIELD_VERSION]}</td>");
        $this->addln("<td>{$precord[self::FIELD_LICENSE]}</td>");
        $this->add("<td><a href=\"{$precord[self::FIELD_URL]}\">");
        $this->add("<img src=\"{$this->minstallericon}\" width=\"{$this->miconsdim}\" height=\"{$this->miconsdim}\">");
        $this->addln("</a></td>");
        if(! isEmpty($precord[self::FIELD_PORTABLE])) {
            
            $this->add("<td><a href=\"{$precord[self::FIELD_PORTABLE]}\">");
            $this->add("<img src=\"{$this->mportableicon}\" width=\"{$this->miconsdim}\" height=\"{$this->miconsdim}\">");
            $this->addln("</a></td>");
        } else {

            $this->addln("<td></td>");
        }
        $this->addln("</tr>");
    }
    
    public function emptyrow($prow) {
        parent::clean();
        if($prow%2>0) {
            
            $this->addln("<tr id=\"{$this->moddlineid}\">");
        } else {

            $this->addln("<tr id=\"{$this->mevenlineid}\">");
        }
        $this->addln("<td>&nbsp</td>");
        $this->addln("<td>&nbsp</td>");
        $this->addln("<td>&nbsp</td>");
        $this->addln("<td>&nbsp</td>");
        $this->addln("<td>&nbsp</td>");
        $this->addln("</tr>");
    }
    
}
?>
