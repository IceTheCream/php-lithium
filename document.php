<?php

class CDocument {
    
    const LN="\n";
    
    private $mln="";
    private $mresult="";
    
    
    public function __construct() {
      
    }


    public function add($pline) {
        
        $this->mresult=$this->mresult.$pline;
    }


    public function addln($pline) {
        
        $this->mresult=$this->mresult.$pline;
        $this->mresult=$this->mresult.$this->mln;    
    }  
    
    
    public function display() {
        
        echo($this->mresult);            
    }    


    public function get() {
            
        return $this->mresult;    
    }    


    public function setCondensed($pcondensed=0) {
        
        $this->mln=($pcondensed)?self::LN:"";
    }
    
    public function clean() {
        
        $this->mresult="";
    }
    
}    
?>
