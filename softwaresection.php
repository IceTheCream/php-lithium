<?php 

class CSoftwareSectionBuilder extends CDocument{
    
    private $mxmlname="";
    private $mclassname="";
    private $mheaderclass="";
    private $msqlselect="";
    private $msqlcount="";
    private $mconnect=null;
    private $Query=null;
    private $mcodepage="";
    private $msectionid=0;
    private $mrows=0;
    private $mcolumns=0;
    private $mcolwidthes=Array();
    
    public function __construct($pconnect,$pxmlname) {
        
        parent::__construct();
        $this->mxmlname=$pxmlname;
        $this->mconnect=$pconnect;
        $this->readConfig(); 
        $this->Query=new CDBQuery($this->mconnect,$this->mcodepage); 
    }
    
    
    private function readConfig() {
        
         $xml=simplexml_load_file($this->mxmlname);
        $this->mclassname=$xml->section->classname;
        $this->mheaderclass=$xml->section->headerclass;
        $this->mcolumns=$xml->section->table->columns;
        for($idx=0;$idx<$this->mcolumns;$idx++) {
            $this->mcolwidthes[$idx]=$xml->section->table->column[$idx];
        }
        $this->msqlselect=$xml->section->sqlselect;
        $this->msqlcount=$xml->section->sqlcount;
        $this->mcodepage=$xml->codepage;
        parent::setCondensed($xml->condensed);
        unset($xml);    
   }
    
    public function askRows($psectionid) {
        
        if($this->Query->open($this->msqlcount.$this->msectionid)) {

            $reccount=$this->Query->recordCount();
            $this->Query->close();
        }
    }
    
    public function build($psectionid,$prows) {
        
        $this->msectionid=$psectionid;
        $this->mrows=$prows;
        parent::clean();

        //$this->Query=new CDBQuery($this->mconnect,$this->mcodepage); 
        //dout($this->msqlselect);
        if($this->Query->open($this->msqlselect.$this->msectionid)) {

            $this->addln("<table class=\"{$this->mclassname}\">");    
            $this->addln("<colgroup>");
            for($idx=0;$idx<$this->mcolumns;$idx++) {

                $this->addln("<col width=\"{$this->mcolwidthes[$idx]}\">");
            }        
            $this->addln("</colgroup>");
            $this->addln("<thead>");
            $this->addln("<tr><th colspan=\"{$this->mcolumns}\" class=\"{$this->mheaderclass}\"></th></tr>");
            $this->addln("</thead>");
            $this->addln("<tbody>");
            $this->addln("");
            
            $reccount=$this->Query->recordCount();
            //$emptyrows=$this->mrows-$reccount;
            
            $ItemBuilder=new CSoftwareItemBuilder($this->mxmlname);
            for($idx=0;$idx<$reccount;$idx++) {

                $record=$this->Query->getRecord($idx);
                $ItemBuilder->build($record,$idx);
                $this->addln($ItemBuilder->get());
            }
            for($idx=$reccount;$idx<$this->mrows;$idx++) {
                
                $ItemBuilder->emptyrow($idx);
                $this->addln($ItemBuilder->get());    
            }
            $this->addln("</tbody>");
            $this->addln("</table>");
            $this->addln("");
        }    
    }
}    

?>
