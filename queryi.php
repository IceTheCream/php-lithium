<?php

class CDBQuery  {

    //private $Connection=null;
    //private $mqueryid=null;

    private $mysqli=null;
    
    private $maddress="";
    private $mdatabase="";
    private $muser="";
    private $mpassword="";
    private $codepage="utf8";
    
    //private $mhandle;
    private $merrormsg="";
    
      
    public function __construct() {
        
        //$this->Connection->query("set names ".$pcodepage);
    }
    
    
    public function setAddress($paddress) {
 
        $this->maddress=$paddress;
    }
    
    
    public function setDatabase($pdatabase) {
 
        $this->mdatabase=$pdatabase;   
    }
    
    
    public function setUser($puser) {
  
        $this->muser=$puser;
    }


    public function setPassword($ppassword) {
 
        $this->mpassword=$ppassword;    
    }

    
    public function open($pSQL) {
    
        $this->mysqli=new mysqli($this->maddress,$this->muser,$this->mpassword,$this->mdatabase);
        
        
        return $this->mysqli;
    }

/**
  Подключение к серверу MySQL 
$mysqli = mysqli_connect( 
            'localhost',  /* Хост, к которому мы подключаемся *
            'user',       /* Имя пользователя *
            'password',   /* Используемый пароль *
            'world');     /* База данных для запросов по умолчанию *

if (!$mysqli) { 
   printf("Невозможно подключиться к базе данных. Код ошибки: %s\n", mysqli_connect_error()); 
   exit; 
} 

/ Посылаем запрос серверу *
if ($result = mysqli_query($mysqli, 'SELECT Name, Population FROM City ORDER BY Population DESC LIMIT 5')) { 

    print("Очень крупные города:\n"); 

    * Выборка результатов запроса *
    while( $row = mysqli_fetch_assoc($result) ){ 
        printf("%s (%s)\n", $row['Name'], $row['Population']); 
    } 

    * Освобождаем используемую память *
    mysqli_free_result($result); 
} 

 Закрываем соединение *
mysqli_close($mysqli); 
 * /
    
    public function close() {
    
        mysql_free_result($this->mqueryid);
    }
    
    
    public function recordCount() {
    
        return mysql_num_rows($this->mqueryid);
    }
  
    
    public function getRecord() {
    
        return mysql_fetch_array($this->mqueryid);
    }
    
    
}

?>
